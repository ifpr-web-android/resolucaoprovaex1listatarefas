package com.example.everaldo.resolucaoprovaex1listatarefas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NovaTarefaActivity extends AppCompatActivity {

    private int idTarefa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_tarefa);

        Intent criarNovaTarefa = getIntent();
        idTarefa = criarNovaTarefa.getIntExtra(MainActivity.TAREFA_ID, -1);

        TextView idTarefaTextView = (TextView) findViewById(R.id.idTarefa);
        idTarefaTextView.setText(Integer.toString(idTarefa));



    }

    public void criarTarefa(View view){
        EditText nomeTarefaEditText = (EditText) findViewById(R.id.nomeTarefa);
        String nomeTarefa = nomeTarefaEditText.getText().toString();

        if("".equals(nomeTarefa)){
            Toast.makeText(this, "O nome não pode ser vazio", Toast.LENGTH_SHORT).show();
            nomeTarefaEditText.setError("O nome não pode ser vazio");
            return;
        }


        Intent data = new Intent();
        data.putExtra(MainActivity.TAREFA_ID, idTarefa);
        data.putExtra(MainActivity.TAREFA_NOME, nomeTarefa);
        setResult(RESULT_OK, data);
        finish();
    }

}
