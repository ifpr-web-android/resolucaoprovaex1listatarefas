package com.example.everaldo.resolucaoprovaex1listatarefas;

/**
 * Created by everaldo on 06/07/17.
 */

public class Tarefa {

    private int id;
    private String nome;
    private boolean feita;

    public Tarefa(){

    }

    public Tarefa(int id, String nome, boolean feita){
        this.id = id;
        this.nome = nome;
        this.feita = feita;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public boolean isFeita() {
        return feita;
    }

    public void setFeita(boolean feita) {
        this.feita = feita;
    }
}
