package com.example.everaldo.resolucaoprovaex1listatarefas;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by everaldo on 07/07/17.
 */

public class TarefaAdapter extends BaseAdapter {

    List<Tarefa> tarefas;
    MainActivity activity;
    LayoutInflater inflater;


    public TarefaAdapter(MainActivity activity, List<Tarefa> tarefas){
        this.activity = activity;
        this.inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.tarefas = tarefas;
    }

    public void setTarefas(List<Tarefa> tarefas){
        this.tarefas = tarefas;
        notifyDataSetChanged();
    }

    public void removerTarefa(int position){
        this.tarefas.remove(position);
        notifyDataSetChanged();
    }

    public void adicionarTarefa(Tarefa tarefa){
        tarefas.add(tarefa);
        notifyDataSetChanged();
    }

    public void atualizarTarefa(int position, Tarefa tarefaAtualizada){
        this.tarefas.set(position, tarefaAtualizada);
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        if(tarefas == null){
            return 0;
        }
        return tarefas.size();
    }

    @Override
    public Object getItem(int position) {
        if(tarefas == null || tarefas.get(position) == null){
            return null;
        }
        return tarefas.get(position);
    }

    @Override
    public long getItemId(int position) {
        if(tarefas == null || tarefas.get(position) == null){
            return -1;
        }
        return tarefas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = inflater.inflate(R.layout.item_tarefa, parent, false);
        }

        final Tarefa tarefa = tarefas.get(position);

        if(tarefa != null){
            TextView idTarefa = (TextView) convertView.findViewById(R.id.item_id_tarefa);
            idTarefa.setText(Integer.toString(tarefa.getId()));
            TextView nomeTarefa = (TextView) convertView.findViewById(R.id.item_nome_tarefa);
            nomeTarefa.setText(tarefa.getNome());
            if(tarefa.isFeita()){
                nomeTarefa.setPaintFlags(nomeTarefa.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }
            else{
                nomeTarefa.setPaintFlags(nomeTarefa.getPaintFlags() & (~Paint.STRIKE_THRU_TEXT_FLAG));
            }
        }

        return convertView;
    }
}
