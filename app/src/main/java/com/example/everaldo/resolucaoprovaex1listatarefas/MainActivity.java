package com.example.everaldo.resolucaoprovaex1listatarefas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String TAREFA_ID = "TAREFA_ID";
    public static final int REQUEST_NOVA_TAREFA = 1;
    public static final String TAREFA_NOME = "TAREFA_NOME";
    public static final String TAREFA_FEITA = "TAREFA_FEITA";
    private static final int REQUEST_EDITAR_TAREFA = 2;
    public static final String TAREFA = "TAREFA";
    public static final String TAREFA_INDEX = "TAREFA_INDEX";
    private int idTarefas = 0;
    private TarefaAdapter tarefaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tarefaAdapter = new TarefaAdapter(this, new ArrayList<Tarefa>());
        ListView listaTarefas = (ListView) findViewById(R.id.listaTarefas);
        listaTarefas.setAdapter(tarefaAdapter);
        listaTarefas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent editarTarefa = new Intent(MainActivity.this, EditarTarefaActivity.class);
                Tarefa tarefa = (Tarefa) tarefaAdapter.getItem(position);
                editarTarefa.putExtra(TAREFA_INDEX, position);
                editarTarefa.putExtra(TAREFA_ID, tarefa.getId());
                editarTarefa.putExtra(TAREFA_NOME, tarefa.getNome());
                editarTarefa.putExtra(TAREFA_FEITA, tarefa.isFeita());
                startActivityForResult(editarTarefa, REQUEST_EDITAR_TAREFA);
            }
        });

        listaTarefas.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                tarefaAdapter.removerTarefa(position);
                return true;
            }
        });

    }



    public void novaTarefa(View view){
        idTarefas++;
        Intent criarNovaTarefa = new Intent(this, NovaTarefaActivity.class);
        criarNovaTarefa.putExtra(TAREFA_ID, idTarefas);
        startActivityForResult(criarNovaTarefa, REQUEST_NOVA_TAREFA);
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_NOVA_TAREFA && resultCode == RESULT_OK){
            Tarefa tarefa = new Tarefa(data.getIntExtra(MainActivity.TAREFA_ID, -1),
                    data.getStringExtra(MainActivity.TAREFA_NOME), false);
            tarefaAdapter.adicionarTarefa(tarefa);
        }
        else if(requestCode == REQUEST_EDITAR_TAREFA && resultCode == RESULT_OK){
            Tarefa tarefa = new Tarefa(data.getIntExtra(MainActivity.TAREFA_ID, -1),
                    data.getStringExtra(MainActivity.TAREFA_NOME), data.getBooleanExtra(
                            MainActivity.TAREFA_FEITA, false));
            int index = data.getIntExtra(MainActivity.TAREFA_INDEX, -1);
            tarefaAdapter.atualizarTarefa(index, tarefa);
        }
    }

}
