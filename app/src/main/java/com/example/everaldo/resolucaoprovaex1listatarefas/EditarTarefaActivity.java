package com.example.everaldo.resolucaoprovaex1listatarefas;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class EditarTarefaActivity extends AppCompatActivity {

    private int index;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_tarefa);

        Intent data = getIntent();

        index = data.getIntExtra(MainActivity.TAREFA_INDEX, -1);

        int id = data.getIntExtra(MainActivity.TAREFA_ID, -1);
        String nomeTarefa = data.getStringExtra(MainActivity.TAREFA_NOME);
        boolean feita = data.getBooleanExtra(MainActivity.TAREFA_FEITA, false);

        TextView idTarefa = (TextView) findViewById(R.id.idTarefa);
        idTarefa.setText(Integer.toString(id));

        EditText nomeTarefaEditText = (EditText) findViewById(R.id.nomeTarefa);
        nomeTarefaEditText.setText(nomeTarefa);

        CheckBox tarefaFeitaCheckbox = (CheckBox) findViewById(R.id.feitaTarefa);
        tarefaFeitaCheckbox.setChecked(feita);


    }


    public void atualizarTarefa(View view){
        TextView idTarefa = (TextView) findViewById(R.id.idTarefa);
        EditText nomeTarefaEditText = (EditText) findViewById(R.id.nomeTarefa);
        CheckBox tarefaFeitaCheckbox = (CheckBox) findViewById(R.id.feitaTarefa);

        int id = Integer.parseInt(idTarefa.getText().toString());
        String nome = nomeTarefaEditText.getText().toString();
        boolean feita = tarefaFeitaCheckbox.isChecked();

        if("".equals(nome)){
            Toast.makeText(this, "O nome não pode ser vazio", Toast.LENGTH_SHORT).show();
            nomeTarefaEditText.setError("O nome não pode ser vazio");
            return;
        }

        Intent data = new Intent();
        data.putExtra(MainActivity.TAREFA_INDEX, index);
        data.putExtra(MainActivity.TAREFA_ID, id);
        data.putExtra(MainActivity.TAREFA_NOME, nome);
        data.putExtra(MainActivity.TAREFA_FEITA, feita);

        setResult(RESULT_OK, data);
        finish();

    }

}
